# Prueba de Java & Maven

## Objetivo principal

- Hacer ajustes en una aplicación Java Spring Boot, para proporcionar un mock server a la aplicación AngularJS.

## Aspecto a evaluar

- Gestión de dependencias con Maven
- Use de colecciones en Java
- Uso de Java Streams
- Manejo de git, ramificación en git
- Creación de *pull request* en Bitbucket
- Cambiar el mock de una apl

## Prerrequisitos

- JDK 11
- Cualquier editor de texto

## Configuración inicial

- Importar este repositorio en su cuenta personal de Bitbucket
- El nombre del repositorio debe ser `bg-training-maven`
- El nivel de acceso al repositorio debe estar privado
- Otorgar acceso de administrador a la siguiente cuenta de correo electrónico `cicaza@consultec-ti.com`

## Actividades

1. Crear las ramas `develop` y `documents` a partir de la rama `master`
2. Crear la rama `feature/implement_service` a partir de la rama `develop`
3. Crear la rama `feature/docker` a partir de la rama `develop`
4. Importar la dependencia generada por el proyecto `bg-training-gradle`
5. Crear los *pull request* hacia la rama `develop`

### Parte 1

Implementar el método `getLoans()`

Este método deve devolver una lista de Loans.

Versionar los cambios del proyecto en la rama `feature/implement_service`

### Parte 2

Implementar el método `getLoanById(String id)`

Este método debe devolver un loan, el cual se obtiene filtrando la lista de Loans global, el filtro se debe hacer el por atributo id.

Este método desde Postman se consume así `http://localhost:8086/api/products/loan/200-0-003`, lo que significa que al parámetro `id` tendrá en valor igual a `200-0-003`

Versionar los cambios del proyecto en la rama `feature/implement_service`

### Parte 3

El equipo QA de BG requiere que distribuya la aplicación en una imagen de Docker.

- Crear el archivo `Dockerfile` en la raíz del proyecto
- Crear una imagen a partir del `Dockerfile`
- Subir la imagen a su cuenta en Docker Hub
- Versionar los cambios del proyecto en la rama `feature/docker`
- Mezclar los cambios de la rama `feature/implement_service` en la rama `feature/docker`
- Indicar en el *pull request* el nombre de la imagen en Docker Hub

### Parte 4

El equipo DevOps de BG requiere que la aplicación sea compilada usando Gradle.

- Crear la rama `feature/gradle` a partir de `develop`
- Mezclar la rama `feature/implement_service` en `feature/gradle`
- Mezclar la rama `feature/docker` en `feature/gradle`
- Agregar los archivos necesarios para compilar y ejecutar la aplicación
- Construir la imagen Docker y ejecute el contenedor para probar la aplicación compilada por Gradle

### Notas
- Considere usar una lista de loans `public static`, que le permita operar sobre la misma en ambos métodos
- Opcional: agregar un archivo .json con la información de los loans (el mismo que está en el proyecto AngularJS/Stubby) y usando la librería GSon, leer ese archivo de classpath, crear la lista y operar sobre la misma.

## Tiempo estimado
- 180 minutos

---

## Información del proyecto

### Framework

- Spring Boot 2.5.2

### Herramientas

- Java 11
- Maven Wrapper 3.8.1

### Dependencias

- Lombok
- Spring Boot DevTools
- Spring Web

## Información del API

- Port 8086
- Context Path = /api/products
- GET /loans
- GET /loan/id

## Construcción del proyecto

### Compilación usando Maven Wrapper
```
$ ./mvnw clean package
```

### Ejecución usando Maven Wrapper
```
$ ./mvnw spring-boot:run
```

### Ejecución usando Java
```
$ java -jar target/loan-service-1.0.0-SNAPSHOT.jar
```
